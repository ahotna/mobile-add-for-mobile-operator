import React, { useState } from "react";
import { StyleSheet, View } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { NavigationOnPages } from "./src/navigationFiles/NavigationLogin";
import { UserContext } from "./src/context/user";
import { TariffContext } from "./src/context/currectTariff";
import * as Font from "expo-font";
import AppLoading from "expo-app-loading";

async function loadAppAplication() {
  await Font.loadAsync({
    "roboto-regular": require("./src/fonts/Roboto-Regular.ttf"),
    "roboto-bold": require("./src/fonts/Roboto-Bold.ttf"),
  });
}
export default function App() {
  const [user, setUser] = useState({
    id: "",
  });
  const [tariff, setTariff] = useState({ id: "" });
  const [isReady, setIsReady] = useState(false);

  if (!isReady) {
    return (
      <UserContext.Provider value={{ user, setUser }}>
        <AppLoading
          startAsync={loadAppAplication}
          onError={(err) => console.log(err)}
          onFinish={() => setIsReady(true)}
        />
      </UserContext.Provider>
    );
  }
  return (
    <TariffContext.Provider value={{ tariff, setTariff }}>
      <UserContext.Provider value={{ user, setUser }}>
        <NavigationContainer>
          <NavigationOnPages style={styles.container} />
        </NavigationContainer>
      </UserContext.Provider>
    </TariffContext.Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    fontFamily: "roboto-regular",
  },
  Text: {
    color: "#fff",
  },
});
