import React, { useContext, useEffect, useState } from "react";
import { View, Text, ScrollView, TouchableOpacity } from "react-native";
import { Card, ListItem, Button } from "react-native-elements";
import Icon from "@mdi/react";
import { mdiAlphaSCircleOutline } from "@mdi/js";
import { styles } from "./styles/ItemTariffStyle";
import { TariffContext } from "./context/currectTariff";
import { UserContext } from "./context/user";
import { useNavigation } from "@react-navigation/core";

const users = [
  {
    name: "Турбо",
    desc1: "Безлимитный интернет",
    desc2: "2500 минут  / 500 SMS",
    desc3: "Платит один – пользуются все",
    desc4:
      "Домашний интернет + IPTV и Мобильное ТВ + Мобильная связь для всей семьи",
  },
];

export function ItemTariff() {
  const navigation = useNavigation();
  const tariffContext = useContext(TariffContext);
  const [tar, setTariffs] = useState([]);
  const [tarClient, setTarClient] = useState([]);
  const [idTar, setIdTar] = useState(11111);
  const userContext = useContext(UserContext);
  useEffect(() => {
    fetch(`https://localhost:5001/api/tariff/${tariffContext.tariff.id}`, {
      headers: { "Content-Type": "application/json;charset=utf-8" },
    })
      .then((response) => response.json())
      .then((data) => setTariffs(data))
  }, []);
  useEffect(() => {
    fetch(`https://localhost:5001/api/Tariff/user/${userContext.user.id}`)
      .then((response) => response.json())
      .then((tar) => {
        setTarClient(tar[0]);
      });
  }, []);
  useEffect(() => {
    setIdTar(tar.id)
  }, [tar]);
  const ChangeTar = () => {
    fetch(`https://localhost:5001/api/tariff/connecttar/${userContext.user.id}`, {
      method: 'POST',
      headers: { "Content-Type": "application/json;charset=utf-8" },
      body: JSON.stringify(tar.id),
    })
      .then(() => {
        navigation.navigate("Home")
      })
  };
  return (
    <React.Fragment>
      <ScrollView style={styles.scroll}>
        <Card style={styles.сard}>
          <View style={styles.сard}>
            <Card.Title>{tar.name}</Card.Title>
            <Card.Divider />
            <Text style={styles.container}>
              Стоимость: {tar.subcriptionFee}
            </Text>
            <Text style={styles.container}>
              Стоимость минуты в домашнем регионе: {tar.costOneMinCallCity}
            </Text>
            <Text style={styles.container}>
              Стоимость минуты по России: {tar.costOneMinCallInternation}
            </Text>
            <Text style={styles.container}>
              Стоимость минуты в другую страну: {tar.costOneMinCallOutCity}
            </Text>
            <Text style={styles.container}>
              Стоимость СМС: {tar.costSms}
            </Text>
            <Text style={styles.container}>
              Бесплатных минут: {tar.freeMinuteForMonth}
            </Text>
            <Text style={styles.container}>Бесплатных СМС: {tar.sms}</Text>
            <Text style={styles.container}>
              Бесплатный трафик: {tar.intGb}
            </Text>
          </View>
        </Card>
        {
          // console.log(tarClient.name, tar.name)
          (tarClient.name === tar.name ?
            <TouchableOpacity
              style={styles.submitDisabledButton}
              disabled={true}
              onPress={() => {
                ChangeTar()
              }}
            >
              <Text> Подключен </Text>
            </TouchableOpacity>
            :
            <TouchableOpacity
              style={styles.submitButton}
              onPress={() => {
                ChangeTar()
              }}
            >
              <Text> Подключить </Text>
            </TouchableOpacity>
          )
        }
      </ScrollView>
    </React.Fragment>
  );
}
