import React, { useContext, useEffect, useState } from "react";
import { View, Text, Image, StyleSheet, ScrollView, TouchableOpacity } from "react-native";
import { Card, ListItem, Button } from "react-native-elements";
import Icon from "@mdi/react";
import { mdiPhone } from "@mdi/js";
import { UserContext } from "./context/user";
import { CardStyleInterpolators } from "@react-navigation/stack";
import { TextInput } from "react-native-gesture-handler";
import { styles } from "./styles/AddBalanceStyle";

export function AddBalance() {
  const userContext = useContext(UserContext);
  const [errorMessage, setErrorMessage] = useState([]);
  const [addBalance, setAddBalance] = useState({
    PhoneNumberForAddBalance: "0",
    SumForAdd: "0",
    NumberBankCard: "0",
    NameBankCard: "0",
    CvvbankCard: "0",
    IdClient: userContext.user.id,
    DateBankCard: "01.01.2000",
  });
  let errorArray = [];
  const AddBalance = () => {
    fetch(`https://localhost:5001/api/AddBalance`, {
      method: 'POST',
      headers: { "Content-Type": "application/json;charset=utf-8" },
      body: JSON.stringify(addBalance),
    })
      .then((response) => response.json())
      .then((response) => {
        if (!response.errors) {
          // userContext.setUser((prev) => ({ ...prev, id: res.id }));
          // navigation.navigate("MainScreen");
        }
        else {
          response.errors.CvvbankCard.forEach(element => {
            switch (element) {
              case "Error converting value {null} to type 'System.Int32'. Path 'CvvbankCard', line 1, position 100.":
                errorArray.push("Ошибка преобразования значения {null} в тип «System.Int32». Путь «CvvbankCard», строка 1, позиция 100.");
                break;
              case "Could not convert string to integer: qwe. Path 'CvvbankCard', line 1, position 103.":
                errorArray.push("Не удалось преобразовать строку в целое число: qwe. Путь «CvvbankCard», строка 1, позиция 103.");
                break;
              default: errorArray.push(element);
            }
          });
          response.errors.SumForAdd.forEach(element => {
            switch (element) {
              case "Error converting value {null} to type 'System.Int32'. Path 'SumForAdd', line 1, position 45.":
                errorArray.push("Ошибка преобразования значения {null} в тип «System.Int32». Путь «SumForAdd», строка 1, позиция 45.");
                break;
              default: errorArray.push(element);
            }
          });
          console.log(errorArray);
          setErrorMessage(errorArray);
        }
      })
  };
  const handleNumberPhone = (text) => {
    setAddBalance((prev) => ({
      ...prev,
      PhoneNumberForAddBalance: text,
    }));
  };
  const handleSum = (text) => {
    setAddBalance((prev) => ({
      ...prev,
      SumForAdd: text,
    }));
  };
  const handleNumberBankCard = (text) => {
    setAddBalance((prev) => ({
      ...prev,
      NumberBankCard: text,
    }));
  };
  const handleDateBankCard = (text) => {
    setAddBalance((prev) => ({
      ...prev,
      DateBankCard: text,
    }));
  };
  const handleNameBankCard = (text) => {
    setAddBalance((prev) => ({
      ...prev,
      NameBankCard: text,
    }));
  };
  const handleCvvbankCard = (text) => {
    setAddBalance((prev) => ({
      ...prev,
      CvvbankCard: text,
    }));
  };
  return (
    <React.Fragment>
      <ScrollView style={styles.scroll}>
        <Card>
          <View>
            <Card.Title>Введите данные для пополнения мобильного счета</Card.Title>
            <Card.Divider />
            <TextInput
              style={styles.input}
              underlineColorAndroid="transparent"
              placeholder=" Номер пополнения"
              placeholderTextColor="#000000"
              autoCapitalize="none"
              onChangeText={handleNumberPhone}
            />
            <TextInput
              style={styles.input}
              underlineColorAndroid="transparent"
              placeholder=" Сумма пополнения"
              placeholderTextColor="#000000"
              autoCapitalize="none"
              onChangeText={handleSum}
            />
            <TextInput
              style={styles.input}
              underlineColorAndroid="transparent"
              placeholder=" Номер карты"
              placeholderTextColor="#000000"
              autoCapitalize="none"
              onChangeText={handleNumberBankCard}
            />
            <TextInput
              style={styles.input}
              underlineColorAndroid="transparent"
              placeholder=" Владелец карты"
              placeholderTextColor="#000000"
              autoCapitalize="none"
              onChangeText={handleNameBankCard}
            />
            <TextInput
              style={styles.input}
              underlineColorAndroid="transparent"
              placeholder=" Дата окончания службы карты"
              placeholderTextColor="#000000"
              autoCapitalize="none"
              onChangeText={handleDateBankCard}
            />
            <TextInput
              style={styles.input}
              underlineColorAndroid="transparent"
              placeholder=" CVV"
              placeholderTextColor="#000000"
              autoCapitalize="none"
              onChangeText={handleCvvbankCard}
            />
            {(errorMessage) && (
              errorMessage.map(el => { return (<Text style={styles.errorMessage} key={el}>{el}</Text>) })
            )
            }
            <TouchableOpacity
              style={styles.submitButton}
              onPress={() => {
                AddBalance()
              }}
            >
              <Text> Пополнить баланс </Text>
            </TouchableOpacity>
          </View>
        </Card>
      </ScrollView>
    </React.Fragment>
  );
}
