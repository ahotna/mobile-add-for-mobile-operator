import React, { useContext, useEffect, useState } from "react";
import { View, Text, Image, StyleSheet, ScrollView, TouchableOpacity } from "react-native";
import { Card, ListItem, Button } from "react-native-elements";
import Icon from "@mdi/react";
import { mdiAlphaTCircleOutline, mdiFormatWrapInline } from "@mdi/js";
import { TariffContext } from "./context/currectTariff";
import { styles } from "./styles/TariffStyle";
import { UserContext } from "./context/user";

// const users = [
//   {
//     name: "Для всех",
//     desc: "Подписка на услуги связи, музыку, онлайн-кинотеатры и другие сервисы экосистемы — выберите то, что нужно именно вам",
//     cost: "400р",
//   },
//   {
//     name: "Минимальный",
//     desc: "Все самое лучшее в одной подписке",
//     cost: "200р",
//   },
//   {
//     name: "Для семьи",
//     desc: "Все самое лучшее для ребёнка и семьи в одной подписке",
//     cost: "500р",
//   },
//   {
//     name: "Турбо",
//     desc: "Безлимитный интернет для телефона",
//     cost: "800р",
//   },
// ];

export function Tariffs({ navigation }) {
  const tariffContext = useContext(TariffContext);
  const [tariffList, setTariffsList] = useState([]);
  const [currentTar, setCurrentTar] = useState([]);
  const [disabled, setDisabled] = useState(true);
  const userContext = useContext(UserContext);
  useEffect(() => {
    fetch(`https://localhost:5001/api/tariff`)
      .then((response) => response.json())
      .then((data) => setTariffsList(data));
  }, []);
  // useEffect(() => {
  //   fetch(`https://localhost:5001/api/Tariff/user/${userContext.user.id}`)
  //     .then((response) => response.json())
  //     .then((tar) => {
  //       setCurrentTar(tar[0]);
  //     });
  // }, []);
  return (
    <React.Fragment>
      <ScrollView style={styles.scroll}>
        {tariffList.map((t, i) => (
            <Card key={i}>
              <Card.Title>{t.name}</Card.Title>
              <Card.Divider />
              <View style={styles.viewTextAndImageCard}>
                <View style={styles.img}>
                  <Icon
                    path={mdiAlphaTCircleOutline}
                    title="User Profile"
                    size={2}
                    horizontal
                    vertical
                    rotate={180}
                    color="red"
                  />
                </View>
                <View style={styles.viewText}>
                  <Text style={styles.container}>
                    Бесплатных минут: {t.freeMinuteForMonth}
                  </Text>
                  <Text style={styles.container}>
                    Бесплатного трафика: {t.intGb}
                  </Text>
                  <Text style={styles.container}>Бесплатных смс: {t.sms}</Text>
                </View>
              </View>
              <Text style={styles.costText}>{t.subcriptionFee} руб</Text>
              <TouchableOpacity
                style={styles.submitButton}
                disabled={false}
                onPress={() => {
                  tariffContext.setTariff({ id: t.id });
                  navigation.navigate("Details");
                }}
              >
                <Text> Подробнее... </Text>
              </TouchableOpacity>
            </Card>
        ))}
      </ScrollView>
    </React.Fragment>
  );
}
