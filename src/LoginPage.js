import { useNavigation } from "@react-navigation/core";
import { StatusBar } from "expo-status-bar";
import React, { useContext, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  Button,
  TouchableOpacity,
} from "react-native";
import { UserContext } from "./context/user";
import { styles } from "./styles/LoginPageStyle";

export function LoginPage() {
  const navigation = useNavigation();
  const userContext = useContext(UserContext);
  const [errorMessage, setErrorMessage] = useState([]);
  let errorArray = [];
  const [user, setUser] = useState({
    // LoginPhoneNumber: "", //вернуть на пустые значчения после отладки
    // Password: "",
    LoginPhoneNumber: "89109817270",
    Password: "Aa123456!",
  });
  const handleLogin = (text) => {
    setUser((prev) => ({
      ...prev,
      LoginPhoneNumber: text,
    }));
  };
  const handlePassword = (text) => {
    setUser((prev) => ({
      ...prev,
      Password: text,
    }));
  };
  const login = (LoginPhoneNumber, Password) => {
    fetch(`https://localhost:5001/api/Account/Login`, {
      method: "POST",
      headers: { "Content-Type": "application/json;charset=utf-8" },
      body: JSON.stringify(user),
    })
      .then((response) => response.json())
      .then((res) => {
        if (!res.error) {
          userContext.setUser((prev) => ({ ...prev, id: res.id }));
          navigation.navigate("MainScreen");
        }
        else {
          res.error.forEach(element => {
            switch (element) {
              case "The Логин field is required.": errorArray.push("Поле Логин обязательно для заполнения");
                break;
              case "The Подтвердить пароль field is required.": errorArray.push("Поле Подтвердить пароль обязательно для заполнения");
                break;
              case "The Пароль field is required.": errorArray.push("Поле Пароль обязательно для заполнения");
                break;
              default: errorArray.push(element);
            }
          });
          setErrorMessage(errorArray);
        }
      });
  };
  const registration = () => {
    navigation.navigate("Registration");
  };
  return (
    <View style={styles.container}>
      <Text style={styles.text}>Введите данные аккаунта</Text>
      <TextInput
        style={styles.input}
        underlineColorAndroid="transparent"
        placeholder="Логин"
        placeholderTextColor="#000000"
        autoCapitalize="none"
        onChangeText={handleLogin}
      />

      <TextInput
        style={styles.input}
        underlineColorAndroid="transparent"
        placeholder="Пароль"
        placeholderTextColor="#000000"
        autoCapitalize="none"
        secureTextEntry={true}
        onChangeText={handlePassword}
      />

      <TouchableOpacity
        style={styles.submitButton}
        onPress={() => login(user.LoginPhoneNumber, user.Password)}
      >
        <Text style={styles.submitButtonText}> Войти </Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.submitButton}
        onPress={() => registration()}
      >
        <Text style={styles.submitButtonText}> Регистрация </Text>
      </TouchableOpacity>
      {(errorMessage) && (
        errorMessage.map(el => { return (<Text style={styles.errorMessage} key={el}>{el}</Text>) })
      )
      }
    </View>
  );
}
