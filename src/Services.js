import React, { useEffect, useState, useContext } from "react";
import { View, Text, Image, StyleSheet, ScrollView, TouchableOpacity } from "react-native";
import { Card, ListItem, Button } from "react-native-elements";
import { UserContext } from "./context/user";
import Icon from "@mdi/react";
import { mdiAlphaSCircleOutline } from "@mdi/js";
import { styles } from "./styles/ServicesStyle";


// const users = [
//   {
//     name: "Блокировка рекламы",
//     desc: "Избавит от навязчивых баннеров в интернете",
//     cost: "300р",
//   },
//   {
//     name: "Если на счете 0",
//     desc: "Когда нужно оставаться на связи, а возможности пополнить счет нет",
//     cost: "100р",
//   },
//   {
//     name: "Красивый номер",
//     desc: "Выберите красивый номер мобильного телефона, который легко запомнят друзья, родственники и знакомые!",
//     cost: "1000р",
//   },
// ];

export function Services() {
  const userContext = useContext(UserContext);
  const [service, setService] = useState([]);
  const [Name, setName] = useState();

  useEffect(() => {
    fetch(`https://localhost:5001/api/service`)
      .then((response) => response.json())
      .then((data) => setService(data));
  }, []);
  const addService = (Name, idClient) => {
    fetch(`https://localhost:5001/api/service/connectnewser`, {
      method: 'POST',
      headers: { "Content-Type": "application/json;charset=utf-8" },
      body: JSON.stringify({ Name, idClient }),
    })
      // .then((response) => response.json())
  };
  return (
    <React.Fragment>
      <ScrollView style={styles.scroll}>
        {service.map((s, i) =>
        (
          <Card key={s.id}>
            <Card.Title>{s.name}</Card.Title>
            <Card.Divider />
            <View style={styles.viewTextAndImageCard}>
              <Icon
                path={mdiAlphaSCircleOutline}
                title="User Profile"
                size={2}
                horizontal
                vertical
                rotate={180}
                color="red"
              />
              <Text style={styles.container}>{s.description}</Text>
            </View>
            <Text style={styles.costText}>{s.subscFee} руб</Text>
            <TouchableOpacity
              style={styles.submitButton}
              onPress={() => {
                addService(s.name, userContext.user.id)
              }}
            >
              <Text> Подключить </Text>
            </TouchableOpacity>
          </Card>
        )
        )}
      </ScrollView>
    </React.Fragment>
  );
}

