import React, { Component, useState } from "react";
import { StyleSheet, View } from "react-native";

export default function withStyleComponent(children) {
  const styles = StyleSheet.create({
    container: {
      fontFamily: "roboto-regular",
    },
  });
  return <View styles={styles.container}>{children}</View>;
}
