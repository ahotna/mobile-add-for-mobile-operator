import * as React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { FontAwesome } from '@expo/vector-icons'; 
import { AntDesign } from "@expo/vector-icons";
import { Services } from "../Services";
import { Calls } from "../Calls";
import { NavigationOnPages } from "./NavigationInTariffs";
import { PersonalPage } from "../PersonalPage";
import { LoginPage } from "../LoginPage";
import { AddBalance } from "../AddBalance";

const Tab = createBottomTabNavigator();

export function MyTabs() {
  return (
    <Tab.Navigator
      initialRouteName="Main"
      screenOptions={{
        tabBarActiveTintColor: "#e91c1c",
      }}
    >
      <Tab.Screen
        name="Главная"
        component={PersonalPage}
        options={{
          tabBarLabel: "Главная",
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="home" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Тарифы"
        component={NavigationOnPages}
        options={{
          tabBarLabel: "Тарифы",
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="bell" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Услуги"
        component={Services}
        options={{
          tabBarLabel: "Услуги",
          tabBarIcon: ({ color, size }) => (
            <AntDesign name="tagso" size={size} color={color} />
          ),
        }}
      />
      <Tab.Screen
        name="Пополнить"
        component={AddBalance}
        options={{
          tabBarLabel: "Пополнить",
          tabBarIcon: ({ color, size }) => (
            <FontAwesome name="rub" size={size} color={color} />
          ),
        }}
      />
      <Tab.Screen
        name="Звонки"
        component={Calls}
        options={{
          tabBarLabel: "Звонки",
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="account" color={color} size={size} />
          ),
        }}
      />
    </Tab.Navigator>
  );
}
