import * as React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { Tariffs } from "../Tariffs";
import { ItemTariff } from "../itemTariff";
import { MyTabs } from "./TabNavigation";

const Stack = createStackNavigator();

export function NavigationOnPages() {
  return (
    <Stack.Navigator
      initialRouteName="Home"
      screenOptions={{
        headerShown: false,
      }}
    >
      <Stack.Screen
        name="Home"
        component={Tariffs}
        options={{ title: "Home Page" }}
      />
      <Stack.Screen
        name="Details"
        component={ItemTariff}
        options={{ title: "Details Page" }}
      />
    </Stack.Navigator>
  );
}
