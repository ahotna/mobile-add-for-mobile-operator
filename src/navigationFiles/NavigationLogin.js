import * as React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { LoginPage } from "../LoginPage";
import { RegistrationPage } from "../RegistrationPage";
import { MyTabs } from "./TabNavigation";
import withStyleComponent from "../withStyleComponent";

const Stack = createStackNavigator();

export function NavigationOnPages() {
  return (
    <Stack.Navigator
      initialRouteName="Login"
      screenOptions={{
        headerShown: false,
      }}
    >
      <Stack.Screen
        name="Home"
        component={LoginPage}
        options={{ title: "Home Page" }}
      />
      <Stack.Screen
        name="Registration"
        component={RegistrationPage}
        options={{ title: "Registration Page" }}
      />
      <Stack.Screen
        name="MainScreen"
        component={MyTabs}
        options={{ title: "Details Page" }}
      />
    </Stack.Navigator>
  );
}
