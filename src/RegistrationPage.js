import React, { useContext, useEffect, useState } from "react";
import { Text, View, TextInput, Switch, StyleSheet, TouchableOpacity } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { styles } from "./styles/RegistrationStyle";
import { UserContext } from "./context/user";
import { Card } from "react-native-elements";
import SwitchSelector from "react-native-switch-selector";
import DatePicker from 'react-date-picker'
import { useNavigation } from "@react-navigation/core";

const options = [
  { label: "Физическое лицо", value: true },
  { label: "Юридическое лицо", value: false }
];

export function RegistrationPage() {
  const userContext = useContext(UserContext);
  const [errorMessage, setErrorMessage] = useState([]);
  const [DateOfBirthClient, setDateOfBirthClient] = useState(new Date());
  const [dateStartWorkOrganisation, setDateStartWorkOrganisation] = useState(new Date());
  const navigation = useNavigation();
  let errorArray = [];
  // const toggleSwitch = () => setIsEnabled((previousState) => !previousState);
  const [user, setUser] = useState({
    LoginPhoneNumber: "", //вернуть на пустые значчения после отладки
    Password: "",
    NameClient: "",
    nameOrganisation: "",
    SurNameClient: "",
    PasswordConfirm: "",
    PassportClient: "",
    legalAddress: "",
    itn: "",
  });
  const setDefaultUser = () => {
    setUser(() => ({
      LoginPhoneNumber: "", //вернуть на пустые значчения после отладки
      Password: "",
      NameClient: "",
      nameOrganisation: "",
      SurNameClient: "",
      PasswordConfirm: "",
      PassportClient: "",
      legalAddress: "",
      itn: "",
    }));
  };
  const handleLogin = (text) => {
    setUser((prev) => ({
      ...prev,
      LoginPhoneNumber: text,
    }));
  };
  const handlePassword = (text) => {
    setUser((prev) => ({
      ...prev,
      Password: text,
    }));
  };
  const handleName = (text) => {
    setUser((prev) => ({
      ...prev,
      NameClient: text,
    }));
  };
  const handleNameOrganization = (text) => {
    setUser((prev) => ({
      ...prev,
      nameOrganisation: text,
    }));
  };
  const handleSurName = (text) => {
    setUser((prev) => ({
      ...prev,
      SurNameClient: text,
    }));
  };
  const handlePasswordApprove = (text) => {
    setUser((prev) => ({
      ...prev,
      PasswordConfirm: text,
    }));
  };
  const handlePassport = (text) => {
    setUser((prev) => ({
      ...prev,
      PassportClient: text,
    }));
  };
  const handleLegalAddres = (text) => {
    setUser((prev) => ({
      ...prev,
      legalAddress: text,
    }));
  };
  const handleINN = (text) => {
    setUser((prev) => ({
      ...prev,
      itn: text,
    }));
  };
  const [isPhis, setIsPhis] = useState(true);
  const registration = (LoginPhoneNumber, Password, PasswordConfirm, NameClient, SurNameClient, PassportClient, nameOrganisation, legalAddress, itn) => {
    if (isPhis) {
      fetch(`https://localhost:5001/api/Account/register`, {
        method: "POST",
        headers: { "Content-Type": "application/json;charset=utf-8" },
        body: JSON.stringify({ LoginPhoneNumber, Password, PasswordConfirm, isPhis, DateOfBirthClient, NameClient, SurNameClient, PassportClient, nameOrganisation }),
      })
        .then((response) => response.json())
        .then((res) => {
          if (!res.error) {
            userContext.setUser((prev) => ({ ...prev, id: res.id }));
            navigation.navigate("MainScreen");
          }
          else {
            res.error.forEach(element => {
              switch (element) {
                case "The Логин field is required.": errorArray.push("Поле Логин обязательно для заполнения");
                  break;
                case "The Подтвердить пароль field is required.": errorArray.push("Поле Подтвердить пароль обязательно для заполнения");
                  break;
                case "The Пароль field is required.": errorArray.push("Поле Пароль обязательно для заполнения");
                  break;
                default: errorArray.push(element);
              }
            });
            setErrorMessage(errorArray);
          }
        });
    }
    else {
      fetch(`https://localhost:5001/api/Account/register`, {
        method: "POST",
        headers: { "Content-Type": "application/json;charset=utf-8" },
        body: JSON.stringify({ LoginPhoneNumber, Password, PasswordConfirm, isPhis, nameOrganisation, dateStartWorkOrganisation, legalAddress, itn, NameClient }),
      })
        .then((response) => response.json())
        .then((res) => {
          if (!res.error) {
            userContext.setUser((prev) => ({ ...prev, id: res.id }));
            navigation.navigate("MainScreen");
          }
          else {
            res.error.forEach(element => {
              switch (element) {
                case "The Логин field is required.": errorArray.push("Поле Логин обязательно для заполнения");
                  break;
                case "The Подтвердить пароль field is required.": errorArray.push("Поле Подтвердить пароль обязательно для заполнения");
                  break;
                case "The Пароль field is required.": errorArray.push("Поле Пароль обязательно для заполнения");
                  break;
                case "Passwords must be at least 6 characters.": errorArray.push("Пароли должны быть не менее 6 символов");
                  break;
                case "Passwords must have at least one digit ('0'-'9').": errorArray.push("Пароли должны состоять как минимум из одной цифры ('0'-'9')");
                  break;
                case "Passwords must have at least one lowercase ('a'-'z').": errorArray.push("Пароли должны иметь по крайней мере одну строчную букву ('a'-'z')");
                  break;
                case "Passwords must have at least one uppercase ('A'-'Z').": errorArray.push("Пароли должны иметь по крайней мере одну прописную букву ('A'-'Z')");
                  break;
                default: errorArray.push(element);
              }
            });
            setErrorMessage(errorArray);
          }
        });
    }
    // fetch(`https://localhost:5001/api/Account/register`, {
    //   method: "POST",
    //   headers: { "Content-Type": "application/json;charset=utf-8" },
    //   body: JSON.stringify({ LoginPhoneNumber, Password, PasswordConfirm, isPhis,DateOfBirthClient,dateStartWorkOrganisation, NameClient, SurNameClient, PassportClient, nameOrganisation, legalAddress, itn }),
    // })
    // .then((response) => response.json())
    // .then((res) => {
    //   if (!res.error) {
    //     userContext.setUser((prev) => ({ ...prev, id: res.id }));
    //     navigation.navigate("MainScreen");
    //   }
    // });
  };
  const GetBack = () => {
    navigation.navigate("Home");
  };
  return (
    <React.Fragment>
      <View style={styles.container}>
        <Text style={styles.h1}>Создание пользователя</Text>
        <SwitchSelector
          width={"-webkit-fill-available"}
          initial={0}
          onPress={value => {
            setIsPhis(value)
            setDefaultUser()
          }}
          textColor={'#111111'} //'#7a44cf'
          selectedColor={'#111111'}
          buttonColor={'red'}
          borerColor={'#111111'}
          hasPadding
          options={options}
          style={{ width: '90%' }}
        />
        {(isPhis) &&
          <div>
            <TextInput
              style={styles.inputBig}
              underlineColorAndroid="transparent"
              placeholder="Логин"
              placeholderTextColor="#000000"
              autoCapitalize="none"
              onChangeText={handleLogin}
            />
            <TextInput
              style={styles.input}
              underlineColorAndroid="transparent"
              placeholder="Имя"
              placeholderTextColor="#000000"
              autoCapitalize="none"
              onChangeText={handleName}
            />
            <TextInput
              style={styles.input}
              underlineColorAndroid="transparent"
              placeholder="Фамилия"
              placeholderTextColor="#000000"
              autoCapitalize="none"
              onChangeText={handleSurName}
            />
            <TextInput
              style={styles.input}
              underlineColorAndroid="transparent"
              placeholder="Пароль"
              placeholderTextColor="#000000"
              autoCapitalize="none"
              onChangeText={handlePassword}
              secureTextEntry={true}
            />
            <TextInput
              style={styles.input}
              underlineColorAndroid="transparent"
              placeholder="Подтвердить пароль"
              placeholderTextColor="#000000"
              autoCapitalize="none"
              onChangeText={handlePasswordApprove}
              secureTextEntry={true}
            />
            <TextInput
              style={styles.inputBig}
              underlineColorAndroid="transparent"
              placeholder="Серия и номер паспорта"
              placeholderTextColor="#000000"
              autoCapitalize="none"
              onChangeText={handlePassport}
            />
            <View style={styles.datePicker}>
              <DatePicker onChange={setDateOfBirthClient} value={DateOfBirthClient} />
            </View>
            <TouchableOpacity
              style={styles.submitButton}
              onPress={() => registration(user.LoginPhoneNumber, user.Password, user.PasswordConfirm, user.NameClient, user.SurNameClient, user.PassportClient, user.nameOrganisation, user.legalAddress, user.itn)}
            >
              <Text style={styles.submitButtonText}> Создать аккаунт </Text>

            </TouchableOpacity>
            <div style={{
              width: '80%', marginLeft: '5%',
            }}>
              {(errorMessage) && (
                errorMessage.map(el => { return (<Text style={styles.errorMessage} key={el}>{el}</Text>) })
              )
              }
            </div>
            <TouchableOpacity
              style={styles.submitButton}
              onPress={() => GetBack()}
            >
              <Text style={styles.submitButtonText}> Назад </Text>
            </TouchableOpacity>
          </div>
        }
        {(!isPhis) &&
          <div>
            <TextInput
              style={styles.inputBig}
              underlineColorAndroid="transparent"
              placeholder="Логин"
              placeholderTextColor="#000000"
              autoCapitalize="none"
              onChangeText={handleLogin}
            />
            <TextInput
              style={styles.input}
              underlineColorAndroid="transparent"
              placeholder="Название организации"
              placeholderTextColor="#000000"
              autoCapitalize="none"
              onChangeText={handleNameOrganization}
            />
            <TextInput
              style={styles.input}
              underlineColorAndroid="transparent"
              placeholder="Юридический адрес"
              placeholderTextColor="#000000"
              autoCapitalize="none"
              onChangeText={handleLegalAddres}
            />
            <TextInput
              style={styles.input}
              underlineColorAndroid="transparent"
              placeholder="Пароль"
              placeholderTextColor="#000000"
              autoCapitalize="none"
              onChangeText={handlePassword}
              secureTextEntry={true}
            />
            <TextInput
              style={styles.input}
              underlineColorAndroid="transparent"
              placeholder="Подтвердить пароль"
              placeholderTextColor="#000000"
              autoCapitalize="none"
              onChangeText={handlePasswordApprove}
              secureTextEntry={true}
            />
            <TextInput
              style={styles.inputBig}
              underlineColorAndroid="transparent"
              placeholder="ИНН"
              placeholderTextColor="#000000"
              autoCapitalize="none"
              onChangeText={handleINN}
            />
            <View style={styles.datePicker}>
              <DatePicker onChange={setDateStartWorkOrganisation} value={dateStartWorkOrganisation} />
            </View>
            <TouchableOpacity
              style={styles.submitButton}
              onPress={() => registration(user.LoginPhoneNumber, user.Password, user.PasswordConfirm, user.NameClient, user.SurNameClient, user.PassportClient, user.nameOrganisation, user.legalAddress, user.itn)}
            >
              <Text style={styles.submitButtonText}> Создать аккаунт </Text>
            </TouchableOpacity>
            {(errorMessage) && (
              errorMessage.map(el => { return (<Text style={styles.errorMessage} key={el}>{el}</Text>) })
            )
            }
            <TouchableOpacity
              style={styles.submitButton}
              onPress={() => GetBack()}
            >
              <Text style={styles.submitButtonText}> Назад </Text>
            </TouchableOpacity>
          </div>
        }
      </View>
    </React.Fragment>
  );
}
