import React, { useContext, useEffect, useState } from "react";
import { View, Text, Image, StyleSheet, ScrollView } from "react-native";
import { Card, ListItem, Button } from "react-native-elements";
import Icon from "@mdi/react";
import { mdiPhone } from "@mdi/js";
import { UserContext } from "./context/user";
import { styles } from "./styles/CallsStyle";

export function Calls() {
  const userContext = useContext(UserContext);
  const [calls, setCalls] = useState([]);
  useEffect(() => {
    fetch(`https://localhost:5001/api/call/${userContext.user.id}`)
      .then((response) => response.json())
      .then((data) => setCalls(data));
  }, []);

  return (
    <React.Fragment>
      <ScrollView style={styles.scroll}>
        {calls.map((u, i) => (
          <Card key={u.id}>
            <View style={styles.viewTextAndImageCard}>
              <Icon
                path={mdiPhone}
                title="User Profile"
                size={1}
                horizontal
                vertical
                rotate={180}
                color="red"
              />
              <View style={styles.textBox}>
                <Text>Номер собеседника: {u.numberWasCall}</Text>
                <Text style={styles.container}>
                  Стоимость звонка: {u.costCall}
                </Text>
                <Text style={styles.container}>Время звонка: {u.timeTalk}</Text>
                <Text style={styles.container}>Дата звонка: {u.dateCall}</Text>
              </View>
            </View>
          </Card>
        ))}
      </ScrollView>
    </React.Fragment>
  );
}
