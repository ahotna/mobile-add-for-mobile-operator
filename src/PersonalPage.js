import React, { useContext, useEffect, useState } from "react";
import { Text, View } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { styles } from "./styles/PersonalPageStyle";
import { UserContext } from "./context/user";
import { Card } from "react-native-elements";

export function PersonalPage() {
  const [user, setUser] = useState(null);
  const [userTariff, setUserTariff] = useState(null);
  const userContext = useContext(UserContext);
  useEffect(() => {
    fetch(`https://localhost:5001/api/Client/${userContext.user.id}`)
      .then((response) => response.json())
      .then((data) => setUser(data));
    fetch(`https://localhost:5001/api/Tariff/user/${userContext.user.id}`)
      .then((response) => response.json())
      .then((tar) => {
        setUserTariff(tar);
      });
  }, []);
  if (!user || !userTariff) return <>Loading</>;
  return (
    <React.Fragment>
      <View style={styles.body}>
        <Ionicons name="person-circle-outline" size={120} color="red" />
        <Card>
          <View>
            <Text style={styles.textDesc}>
              Пользователь: {user.name} {user.surName}
            </Text>
            <Text style={styles.textDesc}>
              Номер телефона: {user.phoneNumber}
            </Text>
            <Text style={styles.textDesc}>Тариф: {userTariff[0].name} </Text>
            <Text style={styles.textDesc}>
              Баланс пользователя: {user.balance} рублей
            </Text>
          </View>
        </Card>
        <View style={styles.cardConteiner}>
          <Card containerStyle={styles.card}>
            <Card.Title style={styles.textCard}>Интернет</Card.Title>
            <Card.Divider />
            <View>
              <Text style={styles.text}>{user.freeGb} гб</Text>
            </View>
          </Card>
          <Card containerStyle={styles.card}>
            <Card.Title style={styles.textCard}>Звонки</Card.Title>
            <Card.Divider />
            <View>
              <Text style={styles.text}>{user.freeMin} мин</Text>
            </View>
          </Card>
          <Card containerStyle={styles.card}>
            <Card.Title style={styles.textCard}>Смс</Card.Title>
            <Card.Divider />
            <View>
              <Text style={styles.text}>{user.freeSms} смс</Text>
            </View>
          </Card>
        </View>
      </View>
    </React.Fragment>
  );
}
