import { mdiBlockHelper } from "@mdi/js";
import { StyleSheet, Dimensions } from "react-native";

const windowWidth = Dimensions.get("window").width;

export const styles = StyleSheet.create({
  body: {},
  userInfo: {
    paddingLeft: "10px",
    display: "flex",
    flexDirection: "row",
    flexWrap: "wrap",
  },
  cardConteiner: {
    width: windowWidth,
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: 40,
  },
  card: {
    fontFamily: "roboto-regular",
    width: windowWidth / 3.5,
    marginHorizontal: 10,
  },
  textCard: {
    fontWeight: "bold",
    fontSize: 16,
    textAlign: "center",
    fontFamily: "roboto-regular",
  },
  text: {
    fontSize: 14,
    textAlign: "center",
    fontFamily: "roboto-regular",
  },
  textDesc: {
    fontSize: 14,
    fontFamily: "roboto-regular",
  },
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  input: {
    padding: "10px",
    marginLeft: 18,
    marginTop: 25,
    height: 40,
    borderColor: "red",
    borderWidth: 2,
    borderRadius: '5px',
    fontFamily: "roboto-regular",
    width: '43%'
  },
  inputBig: {
    padding: "10px",
    marginLeft: 18,
    marginTop: 25,
    height: 40,
    borderColor: "red",
    borderWidth: 2,
    borderRadius: '5px',
    width: '91%',
    fontFamily: "roboto-regular"
  },
  submitButton: {
    textAlign: "center",
    backgroundColor: "red",
    padding: 10,
    marginLeft: 18,
    marginTop: 25,
    height: 40,
    width: '91%',
    borderRadius: "5px",
  },
  datePicker: {
    width: '91%',
    borderColor: "red",
    borderWidth: 2, borderRadius: '5px',
    marginLeft: 18,
    marginTop: 25,
  },
  h1: {
    paddingTop: 23,
    margin: 15,
    fontWeight: "bold",
    fontSize: 20,
    textAlign: "center",
    fontFamily: "roboto-regular",
  },
  errorMessage: {
    paddingTop: "5px",
    display: "block",
    fontWeight: "bold",
    fontSize: 14,
    fontFamily: "roboto-regular",
  },
});
