import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
    user: {},
    submitButton: {
      textAlign: "center",
      enabled: "false",
      backgroundColor: "red",
      padding: 10,
      height: 40,
      borderRadius: "5px",
    },
    container: {
      paddingLeft: "10px",
      width: "80%",
    },
    costText: {
      padding: "8px",
      fontWeight: "bold",
    },
    image: {
      display: "flex",
    },
    viewTextAndImageCard: {
      flexDirection: "row",
    },
    img: {
      minWidth: "3rem",
      minHeight: "3rem",
    },
    viewText: {
      width: "100%",
    },
    scroll: {
      paddingBottom: "10px",
    },
  });
  