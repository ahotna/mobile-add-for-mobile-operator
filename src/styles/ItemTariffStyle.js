import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  user: {},
  container: {
    paddingLeft: "10px",
    width: "80%",
  },
  costText: {
    padding: "8px",
    fontWeight: "bold",
  },
  image: {
    display: "flex",
  },
  viewTextAndImageCard: {
    flexDirection: "row",
    flexWrap: "wrap",
  },
  card: {
    width: "100%",
  },
  scroll: {
    paddingBottom: "10px",
  },
  submitButton: {
    textAlign: "center",
    backgroundColor: "red",
    padding: 10,
    marginLeft: 18,
    marginTop: 25,
    height: 40,
    width: '91%',
    borderRadius: "5px",
  },
  submitDisabledButton: {
    textAlign: "center",
    backgroundColor: "grey",
    padding: 10,
    marginLeft: 18,
    marginTop: 25,
    height: 40,
    width: '91%',
    borderRadius: "5px",
  },
});
