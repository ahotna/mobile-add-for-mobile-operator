import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
    user: {},
    container: {
      paddingLeft: "10px",
      width: "80%",
    },
    costText: {
      padding: "8px",
      fontWeight: "bold",
    },
    image: {
      display: "flex",
    },
    viewTextAndImageCard: {
      flexDirection: "row",
      flexWrap: "wrap",
    },
    scroll: {
      paddingBottom: "10px",
    },
    textBox: {
      width: "100%",
    },
  });
  