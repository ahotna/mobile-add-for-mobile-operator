import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
    user: {},
    container: {
      paddingLeft: "10px",
      width: "80%",
    },
    costText: {
      padding: "8px",
      fontWeight: "bold",
    },
    image: {
      display: "flex",
    },
    viewTextAndImageCard: {
      flexDirection: "row",
    },
    scroll: {
      paddingBottom: "10px",
    },
    viewText: {
      width: "100%",
    },
    submitButton: {
      textAlign: "center",
      backgroundColor: "red",
      padding: 10,
      marginLeft: 18,
      marginTop: 25,
      height: 40,
      // width: '91%',
      borderRadius: "5px",
    },
  });
  