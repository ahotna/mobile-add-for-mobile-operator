import { StyleSheet, Dimensions } from "react-native";

const windowWidth = Dimensions.get("window").width;

export const styles = StyleSheet.create({
  body: {},
  userInfo: {
    paddingLeft: "10px",
    display: "flex",
    flexDirection: "row",
    flexWrap: "wrap",
  },
  cardConteiner: {
    width: windowWidth,
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: 40,
  },
  card: {
    fontFamily: "roboto-regular",
    width: windowWidth / 3.5,
    marginHorizontal: 10,
  },
  textCard: {
    fontWeight: "bold",
    fontSize: 16,
    textAlign: "center",
    fontFamily: "roboto-regular",
  },
  text: {
    fontSize: 14,
    textAlign: "center",
    fontFamily: "roboto-regular",
  },
  textDesc: {
    fontSize: 14,
    fontFamily: "roboto-regular",
  },
});
