import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  user: {},
  h1: {
    fontWeight: "bold",
    fontSize: 16,
    fontFamily: "roboto-regular",
  },
  input: { margin: 15, height: 40, borderColor: "red", borderWidth: 1, borderRadius: "5px", fontFamily: "roboto-regular" },
  submitButton: {
    textAlign: "center",
    backgroundColor: "red",
    padding: 10,
    marginLeft: 18,
    marginTop: 25,
    height: 40,
    width: '91%',
    borderRadius: "5px",
  },
});
