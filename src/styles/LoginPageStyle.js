import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  text: {
    paddingTop: 23,
    margin: 15,
    fontWeight: "bold",
    fontSize: 20,
    textAlign: "center",
    fontFamily: "roboto-regular",
  },
  container: {
    flex: 1,
    // alignItems: "center",
    justifyContent: "center",
  },
  input: { padding: "10px", margin: 15, height: 40, borderColor: "red", borderWidth: 1, borderRadius: "5px", fontFamily: "roboto-regular" },
  submitButton: {
    textAlign: "center",
    backgroundColor: "red",
    padding: 10,
    margin: 15,
    height: 40,
    borderRadius: "5px",
  },
  submitButtonText: {
    fontFamily: "roboto-regular",
    color: "#000000",
  },
  errorMessage: {
    marginLeft: "15px",
    paddingTop: "5px",
    display: "block",
    fontWeight: "bold",
    fontSize: 14,
    fontFamily: "roboto-regular",
  }
});
